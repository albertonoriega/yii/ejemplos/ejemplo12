<?php

use app\models\Cliente;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Administración de clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-index">

    <p>
       <?=  Html::a("Tabla", ["indexg"],['class'=>'btn btn-dark text-success m-3']) ?>
    </p>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => "_ver", // vista que se carga por cada registro
        "itemOptions" => [  // itemOptions es para cada elemento
                   'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
                ],
        "options" => [  // options es para todo el componente
                'class' => 'row', // hacemos que todas las cajitas estén en la misma fila
                ],
        'layout' => " {items} {pager}",

    ]) ?>



</div>
