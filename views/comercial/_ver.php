<div class="row">
    <div class="col-lg-12">
        <h2>Id: <?= $model->id ?><br></h2>
        <div class="text-dark bg-info rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->nombre ?></div>
        <div class="text-dark bg-info rounded p-2">Apellido1</div>
        <div class="p-1"><?= $model->apellido1 ?></div>
        <div class="text-dark bg-info rounded p-2">Apellido2</div>
        <div class="p-1"><?= $model->apellido2 ?></div>
        <div class="p-1 mb-5">
            <?php
                //BOTON DE VER
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-eye"></i>', // Icono del botón
                        ['comercial/view', 'id'=> $model->id], //controlador/acción y parametro
                        ['class'=> 'btn btn-primary mr-2']); //estilos del botón   
                
                // BOTON DE ACTUALIZAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-pen-square"></i>',
                        ['comercial/update', 'id'=> $model->id],
                        ['class'=> 'btn btn-success mr-2']);
                
                // BOTON DE ELIMINAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-trash"></i>',
                        ['comercial/delete', 'id' => $model->id],
                        [
                            'class' => 'btn btn-danger mr-2',
                            'data' => [
                                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                                'method' => 'post',
                        ], // esto solo para boton borrar
                ])
            ?>
        </div>
        <br class="float-none">
    </div>
</div>