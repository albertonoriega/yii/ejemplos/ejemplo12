<div class="row">
    <div class="col-lg-12">
        <h2>Id: <?= $model->id ?><br></h2>
        <div class="text-dark bg-info rounded p-2">Total:</div>
        <div class="p-1"><?= $model->total ?></div>
        <div class="text-dark bg-info rounded p-2">Fecha:</div>
        <div class="p-1"><?= $model->fecha ?></div>
        <div class="text-dark bg-info rounded p-2">Id Cliente</div>
        <div class="p-1"><?= $model->id_cliente ?></div>
        <div class="text-dark bg-info rounded p-2">Cliente</div>
        <div class="p-1"><?= $model->cliente->nombreCompleto ?></div>
        <div class="text-dark bg-info rounded p-2">Id Comercial</div>
        <div class="p-1"><?= $model->id_comercial ?></div>
        <div class="text-dark bg-info rounded p-2">Comercial</div>
        <div class="p-1"><?= $model->comercial->nombreCompleto ?></div>
        <div class="p-1 mb-5">
            <?php
                //BOTON DE VER
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-eye"></i>', // Icono del botón
                        ['pedido/view', 'id'=> $model->id], //controlador/acción y parametro
                        ['class'=> 'btn btn-primary mr-2']); //estilos del botón   
                
                // BOTON DE ACTUALIZAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-pen-square"></i>',
                        ['pedido/update', 'id'=> $model->id],
                        ['class'=> 'btn btn-success mr-2']);
                
                // BOTON DE ELIMINAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-trash"></i>',
                        ['pedido/delete', 'id' => $model->id],
                        [
                            'class' => 'btn btn-danger mr-2',
                            'data' => [
                                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                                'method' => 'post',
                        ], // esto solo para boton borrar
                ])
            ?>
        </div>
        <br class="float-none">
        
    </div>
</div>

