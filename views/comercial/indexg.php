<?php

use app\models\Comercial;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Administar comerciales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comercial-index">

    <p>
       <?=  Html::a("Tarjeta", ["index"],['class'=>'btn btn-dark text-success m-3']) ?>
    </p>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellido1',
            'apellido2',
            'comisión',
            'nombreCompleto',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Comercial $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
