<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Pedido $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="pedido-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'total')->input("number",["placeholder"=> "Introduce total"]) ?>

    <?= $form->field($model, 'fecha')->input("date") ?>

    <?= $form->field($model, 'id_cliente')->dropDownList($model->listarClientes(),
            ['prompt' => "Selecciona cliente",]
            ) ?>

    <?= $form->field($model, 'id_comercial')->listBox($model->listarComerciales()) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
