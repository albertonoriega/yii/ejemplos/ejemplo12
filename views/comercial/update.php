<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Comercial $model */

$this->title = 'Actualizar comercial: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Comerciales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Id :" . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="comercial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
