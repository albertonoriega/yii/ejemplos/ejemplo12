<div class="row">
    <div class="col-lg-12">
        <h2>Id: <?= $model->id ?><br></h2>
        <div class="text-dark bg-info rounded p-2">Nombre:</div>
        <div class="p-1"><?= $model->nombre ?></div>
        <div class="text-dark bg-info rounded p-2">Apellido1</div>
        <div class="p-1"><?= $model->apellido1 ?></div>
        <div class="text-dark bg-info rounded p-2">Apellido2</div>
        <div class="p-1"><?= $model->apellido2 ?></div>
        <div class="p-1 mb-5">
            <?php
                //BOTON DE VER
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-eye"></i>', // Icono del botón
                        ['cliente/view', 'id'=> $model->id], //controlador/acción y parametro
                        ['class'=> 'btn btn-primary mr-2']); //estilos del botón   
                
                // BOTON DE ACTUALIZAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-pen-square"></i>',
                        ['cliente/update', 'id'=> $model->id],
                        ['class'=> 'btn btn-success mr-2']);
                
                // BOTON DE ELIMINAR
                echo \yii\helpers\Html::a(
                        '<i class="fas fa-2x fa-trash"></i>',
                        ['cliente/delete', 'id' => $model->id],
                        [
                            'class' => 'btn btn-danger mr-2',
                            'data' => [
                                'confirm' => '¿Estás seguro que deseas eliminar el registro?',
                                'method' => 'post',
                        ], // esto solo para boton borrar
                ])
            ?>
        </div>
        
        
        <?php
        // EJEMPLO DE UN ACORDEON DE JUI
        // 
//        echo yii\jui\Accordion::widget([
//            'items' => [
//                [
//                    'header' => 'Cliente',
//                    'content' => $model->nombreCompleto
//                ],
//                [
//                    'header' => 'Pedidos',
//                    'headerOptions' => ['tag' => 'h3'],
//                    'content' => \yii\helpers\Html::ul(yii\helpers\ArrayHelper::getColumn($model->pedidos, "total")),
//                    'options' => ['tag' => 'div'],
//                ],
//            ],
//            'options' => ['tag' => 'div'],
//            'itemOptions' => ['tag' => 'div'],
//            'headerOptions' => ['tag' => 'h3'],
//            'clientOptions' => ['collapsible' => true],
//        ]);
//        ?>
        <br class="float-none">
    </div>
</div>

    