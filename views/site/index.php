<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">GESTION DEL ALMACEN</h1>

        <p class="lead">Aplicación para la gestión de clientes, comerciales y pedidos</p>

        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Clientes</h2>

                <p>Tabla para la gestión de los clientes</p>

                
            </div>
            <div class="col-lg-4">
                <h2>Comerciales</h2>

                <p>Tabla donde gestiono los comerciales de la empresa</p>

                
            </div>
            <div class="col-lg-4">
                <h2>Pedidos</h2>

                <p>Administración de pedidos</p>

                
            </div>
        </div>

    </div>
</div>
